checked = false;
$(function() {
  $(document).ready(function() {
    $('html').removeClass('preload');
  });

  if (localStorage.getItem("theme") === 'dark') {
    checked = true;
    $('#change-theme').attr('checked', true);
    $('html').attr('data-theme', 'dark');
    $('body').addClass("theme");
    $('#change-theme').text('Change to Light')
  }

  $('#change-theme').click(function() {
    console.log("test");
    checked = !checked;
    $('body').toggleClass("theme");
    if (checked) {
      localStorage.setItem("theme", "dark");
      $('html').attr('data-theme', 'dark');
      $('#change-theme').text('Change to Light')
    } else {
      localStorage.setItem("theme", "light");
      $('html').removeAttr('data-theme');
      $('#change-theme').text('Change to Dark')
    }
  });
});
