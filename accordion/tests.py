from django.test import TestCase, LiveServerTestCase
from selenium import webdriver

# Create your tests here.
class AccordionFunctionalTest(LiveServerTestCase):
	def setUp(self):
		super().setUp()
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		self.browser = webdriver.Chrome(chrome_options = chrome_options)

	def tearDown(self):
		self.browser.quit()
		super().tearDown()

	def test_accordion(self):
		self.browser.get(self.live_server_url + '/profil-accordion/')

		self.assertIn("accordion-head", self.browser.page_source)

		accordion = self.browser.find_elements_by_class_name('accordion-head')[0]

		self.assertNotIn('accordion-head active', self.browser.page_source)
		accordion.click()
		self.assertIn('accordion-head active', self.browser.page_source)

	def test_change_color(self):
		self.browser.get(self.live_server_url + '/profil-accordion/')

		self.assertIn('id="change-theme"', self.browser.page_source)

		change_theme = self.browser.find_elements_by_id('change-theme')[0]
		print(str(change_theme.text)+" test jajaja")
		# print(str(dir(change_theme))+" test jajaja")
		self.assertNotIn('data-theme="dark"', self.browser.page_source)
		change_theme.click()
		self.assertIn('data-theme="dark"', self.browser.page_source)