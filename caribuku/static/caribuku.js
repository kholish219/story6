$(function() {
	$.ajax({
		type:'GET',
		url:'https://www.googleapis.com/books/v1/volumes?q=kholish',
		datatype:'json',
		success: function(data){
			for (i=0; i<data.items.length; i++) {
				if (data.items[i].volumeInfo.imageLinks !== undefined) {
					let img = $('<td>').append($('<img>').attr({
						'src' : data.items[i].volumeInfo.imageLinks.thumbnail	
					}));
					let judul = $('<td>').text(data.items[i].volumeInfo.title);
					let authors = $('<td>').text(data.items[i].volumeInfo.authors);
					let penerbit = $('<td>').text(data.items[i].volumeInfo.publisher);
					let tr = $('<tr>').append(img, judul, authors, penerbit);
					$('tbody').append(tr);
				} else {
					let img = $('<td>').text('-');	
					let judul = $('<td>').text(data.items[i].volumeInfo.title);
					let authors = $('<td>').text(data.items[i].volumeInfo.authors);
					let penerbit = $('<td>').text(data.items[i].volumeInfo.publisher);
					let tr = $('<tr>').append(img, judul, authors, penerbit);
					$('tbody').append(tr);
				}
			}
		}
	})
});

$(function(){
    $('#button-cari').click(function(){
        console.log($('#input-buku').val());
        $('tbody').empty();

		$.ajax({
			type:'GET',
			url:'https://www.googleapis.com/books/v1/volumes?q='+$('#input-buku').val(),
			datatype:'json',
			success: function(data){
				for (i=0; i<data.items.length; i++) {
					if (data.items[i].volumeInfo.imageLinks !== undefined) {
						// console.log(data.items[i].volumeInfo.imageLinks);
						let img = $('<td>').append($('<img>').attr({
							'src' : data.items[i].volumeInfo.imageLinks.thumbnail	
						}));
						let judul = $('<td>').text(data.items[i].volumeInfo.title);
						let authors = $('<td>').text(data.items[i].volumeInfo.authors);
						let penerbit = $('<td>').text(data.items[i].volumeInfo.publisher);
						let tr = $('<tr>').append(img, judul, authors, penerbit);
						$('tbody').append(tr);
					}
				}
			}
		})

	});
});

