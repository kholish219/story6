from django.test import TestCase, Client
from .views import *
from django.urls import resolve

# Create your tests here.
class CariBukuTest(TestCase):
	def test_url_valid(self):
		response = self.client.get('/caribuku/')
		self.assertEqual(response.status_code, 200)

	def test_view_cari_buku(self):
		response = resolve('/caribuku/')
		self.assertEqual(response.func, caribuku)