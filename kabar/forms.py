from django import forms
from .models import Kabar

class KabarForm(forms.Form):
	status = forms.CharField(max_length=300, 
		widget=forms.TextInput(attrs={'style':'border-radius:10px; height:50px; width: 50vw; font-size:32px'}))