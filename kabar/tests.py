from django.test import TestCase, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Create your tests here.
class KabarTest(TestCase):
	def test_url_valid(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_url_invalid(self):
		response = self.client.get('/gaada')
		self.assertEqual(response.status_code, 404)

	def test_cek_ada_tulisan_apakabar(self):
		response = self.client.get('/')
		self.assertIn("Halo, apa kabar?", response.content.decode())

	def test_redirect(self):
		data = { 'status' : "Happy" }
		response = self.client.post('/', data=data)
		self.assertEqual(response.status_code, 302)

class KabarFunctionalTest(LiveServerTestCase):
	def setUp(self):
		super().setUp()
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		self.browser = webdriver.Chrome(chrome_options = chrome_options)

	def tearDown(self):
		self.browser.quit()
		super().tearDown()

	def test_can_input_form(self):
		self.browser.get(self.live_server_url)
		self.assertInHTML('Halo, apa kabar?', self.browser.page_source)
		status = self.browser.find_element_by_id('id_status')

		status.send_keys('Belajar pepew')
		status.send_keys(Keys.RETURN)