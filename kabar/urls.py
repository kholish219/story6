from django.urls import path
from . import views

app_name = 'kabar'

urlpatterns = [
	path('', views.index, name='index'),
]