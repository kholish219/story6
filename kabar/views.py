from django.shortcuts import render, redirect
from .forms import KabarForm
from .models import Kabar

# Create your views here.
def index(request):
	if request.method == 'POST':
		form = KabarForm(request.POST)
		if form.is_valid():
			status = Kabar(status = form.data['status'])
			status.save()
			return redirect('/')
	else:
		form = KabarForm()

	list_status = Kabar.objects.all().order_by('-time')
	return render(request, 'index.html', {'forms':form, 'list_status':list_status})