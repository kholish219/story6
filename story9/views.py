from django.shortcuts import render, redirect
from django.contrib.auth import logout
# Create your views here.
def logoutView(request):
	logout(request)
	return redirect ('kabar:index')
